import { filterEvenNumbers, filterLengthWith4, filterStartWithA } from '../src/filter'

describe('array filter test', () => {
    test('filterEvenNumbers', () => {
        const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        const result = filterEvenNumbers(numbers)
        expect(result).toEqual([2, 4, 6, 8, 10])
    });

    test('filterLengthWith4', () => {
        const strings = ['apple', 'banana', 'cat', 'dogs', 'eggs']
        const result = filterLengthWith4(strings)
        expect(result).toEqual(['dogs', 'eggs'])
    });

    test('filterStartWithA', () => {
        const strings = ['apple', 'banana', 'cat', 'dogs', 'animals']
        const result = filterStartWithA(strings)
        expect(result).toEqual(['apple', 'animals'])
    });
});