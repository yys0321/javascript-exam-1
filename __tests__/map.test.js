import { addSerialNumber, halfNumbers, spliceNames } from '../src/map'

describe('array map test', () => {
    test('addSerialNumber', () => {
        const fruit = ['apple', 'banana', 'orange', 'mango']
        const result = addSerialNumber(fruit)
        expect(result).toEqual(['1. apple', '2. banana', '3. orange', '4. mango'])
    });

    test('halfNumbers', () => {
        const numbers = [2, 10, 20, 30]
        const result = halfNumbers(numbers)
        expect(result).toEqual([1, 5, 10, 15])
    });

    test('spliceNames', () => {
        const names = ['Anna', 'Betty', 'Cathy', 'Daisy']
        const result = spliceNames(names)
        expect(result).toEqual(['Hello Anna', 'Hello Betty', 'Hello Cathy', 'Hello Daisy'])
    });
});