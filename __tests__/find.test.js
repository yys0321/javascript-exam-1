import { firstGrownUp, firstOrange, firstLengthOver5Name } from '../src/find'

describe('array find test', () => {
    test('firstGrownUp', () => {
        const ages = [20, 12, 3, 5, 24]
        const result = firstGrownUp(ages)
        expect(result).toEqual(20)
    });

    test('firstOrange', () => {
        const fruits = ['apple', 'banana', 'orange', 'grape', 'orange']
        const result = firstOrange(fruits)
        expect(result).toEqual('orange')
    });

    test('firstLengthOver5Name', () => {
        const names = ['Anna', 'Betty', 'Carrie', 'Daisy', 'Nicole']
        const result = firstLengthOver5Name(names)
        expect(result).toEqual('Carrie')
    });
});